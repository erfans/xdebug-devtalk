<?php
/**
 * codekunst GmbH <www.codekunst.com>
 * @author ES <hello@codekunst.com>
 */


function rand_color()
{
    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
}

for ($index = 0; $index < 100; $index++) {
    $color = rand_color();
    $size = ceil(rand(0, 200));
    echo "<div style='background-color: $color; height: ${size}px; width: ${size}px; display: inline-block'></div>";
}
