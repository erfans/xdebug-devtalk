#!/usr/bin/env bash

HOST_NAME=$1
HOST_ALIAS=$2
HOST_IP=$3
PHP_VERSION=$4

echo "Host name: $HOST_NAME"
echo "Host alias: $HOST_ALIAS"
echo "Host ip: $HOST_IP"
echo "PHP Version: $PHP_VERSION"

echo "=================================================="
echo "Setting up Vagrant box with..."
echo "- Ubuntu 18 LTS"
echo "- Apache 2.4"
echo "- PHP $PHP_VERSION"
echo "=================================================="


echo "=================================================="
echo "SET LOCALES"
echo "=================================================="

export DEBIAN_FRONTEND=noninteractive

export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_TYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
locale-gen en_US en_US.UTF-8
dpkg-reconfigure locales


echo "=================================================="
echo "RUN UPDATE"
echo "=================================================="

apt-get update
apt-get upgrade

echo "=================================================="
echo "INSTALLING APACHE"
echo "=================================================="

apt-get -y install apache2

if ! [ -L /var/www ]; then
    rm -rf /var/www
    ln -fs /vagrant /var/www
fi

VHOST=$(cat <<EOF
    <VirtualHost *:80>
        DocumentRoot "/var/www/"
        ServerName $HOST_ALIAS
        <Directory "/var/www">
            AllowOverride All
        </Directory>
    </VirtualHost>
EOF
)
echo "$VHOST" > /etc/apache2/sites-available/000-default.conf

echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/localhost.conf

a2enconf localhost
a2enmod rewrite
service apache2 restart

echo "=================================================="
echo "INSTALLING PHP"
echo "=================================================="

apt-get -y update
LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
apt-get -y update
apt-get -y install php$PHP_VERSION php$PHP_VERSION-cli php$PHP_VERSION-intl php$PHP_VERSION-common

service apache2 restart

echo "=================================================="
echo "CLEANING"
echo "=================================================="
apt-get -y autoremove
apt-get -y autoclean

echo "=================================================="
echo "============= INSTALLATION COMPLETE =============="
echo "=================================================="
echo "# Site: $HOST_ALIAS"
